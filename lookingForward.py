#!/usr/bin/python3

'''
 Saudades imensas, intensamente vividas. Nutridas com carinho.  Regadas com amor.
 Cada segundo que passa é um pequeno alívio.
'''
from datetime import date

today = date.today()
lookingForward = date(2019,12,7)
delta = lookingForward - today

print("Agora faltam SÓ... {} dias ou {} horas \n\
      vai passar feito mágica  ♥  ♥  ♥  ♥  ♥  ♥  ♥ ".format(delta.days, delta.days*24)) 
