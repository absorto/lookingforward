# lookingForward

## A very simple script to count the days until I reunite with the person I love the most

### Run

From a GNU/Linux shell all you need to do in order to run this script is to call it from where you have the downloaded file, for example:
`/home/myusername/Downloads/lookingForward.py`

### Standard Output

After running, the script will return to `STDOUT` how many days and hours are left between the current day and December 7, 2019.
You could, of course, change this date for any date you are looking forward to :)
